package org.mpierce.concurrent.throttle;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Supplier;

class QueuedTask<T> {
    final CompletableFuture<T> externalFuture;
    final Supplier<CompletionStage<T>> supplier;

    QueuedTask(Supplier<CompletionStage<T>> supplier, CompletableFuture<T> externalFuture) {
        this.externalFuture = externalFuture;
        this.supplier = supplier;
    }
}
