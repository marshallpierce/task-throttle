// if this shows red, there is a bug in IntelliJ where having a module-info.java makes the dsl show spurious errors. See https://youtrack.jetbrains.net/issue/IDEA-208447

import java.net.URI
import java.time.Duration

plugins {
    `java-library`
    `maven-publish`
    signing
    id("io.github.gradle-nexus.publish-plugin") version "1.0.0"
    id("net.researchgate.release") version "2.8.1"
    id("com.github.ben-manes.versions") version "0.38.0"
}

repositories {
    mavenCentral()
}

val deps by extra {
    mapOf(
        "junit" to "5.7.1"
    )
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testImplementation("org.junit.jupiter:junit-jupiter-params:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
    withSourcesJar()
    withJavadocJar()
}

tasks {
    test {
        useJUnitPlatform()
    }

    afterReleaseBuild {
        dependsOn(provider { project.tasks.named("publishToSonatype") })
    }
}

group = "org.mpierce.concurrent.throttle"

publishing {
    publications {
        register<MavenPublication>("sonatype") {
            from(components["java"])

            // sonatype required pom elements
            pom {
                name.set("${project.group}:${project.name}")
                description.set(name)
                url.set("https://bitbucket.org/marshallpierce/task-throttle")
                licenses {
                    license {
                        name.set("Copyfree Open Innovation License 0.6")
                        url.set("https://copyfree.org/content/standard/licenses/coil/license.txt")
                    }
                }
                developers {
                    developer {
                        id.set("marshallpierce")
                        name.set("Marshall Pierce")
                        email.set("575695+marshallpierce@users.noreply.github.com")
                    }
                }
                scm {
                    connection.set("scm:git:https://bitbucket.org/marshallpierce/task-throttle")
                    developerConnection.set("scm:git:ssh://git@bitbucket.org:marshallpierce/task-throttle.git")
                    url.set("https://bitbucket.org/marshallpierce/task-throttle")
                }
            }
        }
    }

    // A safe throw-away place to publish to:
    // ./gradlew publishSonatypePublicationToLocalDebugRepository -Pversion=foo
    repositories {
        maven {
            name = "localDebug"
            url = URI.create("file:///${project.buildDir}/repos/localDebug")
        }
    }
}

// don't barf for devs without signing set up
if (project.hasProperty("signing.keyId")) {
    signing {
        sign(project.extensions.getByType<PublishingExtension>().publications["sonatype"])
    }
}

nexusPublishing {
    repositories {
        sonatype {
            // sonatypeUsername and sonatypePassword properties are used automatically
            stagingProfileId.set("ab8c5618978d18") // org.mpierce
        }
    }
    // these are not strictly required. The default timeouts are set to 1 minute. But Sonatype can be really slow.
    // If you get the error "java.net.SocketTimeoutException: timeout", these lines will help.
    connectTimeout.set(Duration.ofMinutes(3))
    clientTimeout.set(Duration.ofMinutes(3))
}
